#!/bin/bash

#recebe o arquivo de tags e retorna a media de tempo entre as relesesem dias
#$HOME/scripts/script2.sh para chamar esse script em outro que o executara em todos os arquivos tag

count=0
while read line 
do  
   
   dates[$count]=`echo $line | cut -c1-10`
   
   count=$((count+1))
done < $1

avg=0
for (( i=0; i <= $((count-2)); i++))
do
    avg=$((avg+$(( ( $(date -ud ${dates[$i]} +'%s') - $(date -ud ${dates[$((i+1))]} +'%s') )/60/60/24 ))))
   
done

avg=$((avg/$count))

echo $avg >> "/home/gleison/CloneProjects/avgTime_count.txt"

echo $(( ( $(date -ud ${dates[0]} +'%s') - $(date -ud ${dates[$((count-1))]} +'%s') )/60/60/24 )) >> "/home/gleison/CloneProjects/timeLife_count.txt"
