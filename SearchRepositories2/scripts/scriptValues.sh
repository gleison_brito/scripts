#!/bin/bash

#recebe a lista de pastas de projetos como parametros
while read line 
do  
   cd "$line" 
   echo $line
   git ls-files | wc -l >> "/home/gleison/CloneProjects/NOF.txt"
   git branch -r | wc -l >> "/home/gleison/CloneProjects/branches_count.txt"
   git log --tags --simplify-by-decoration --pretty="format:%ci %d" | wc -l >> "/home/gleison/CloneProjects/releases_count.txt"
   cd ..
done < $1
