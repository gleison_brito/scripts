#!/bin/bash

#recebe lista 

while read line 
do 
   cd "/home/gleison/Projetos/"$line"/"$line"_last"
   last=`git log --format="%ci %d" | head -1 | cut -c1-10` #ultima data
   first=`git log --format="%ci %d"  | tail -1 | cut -c1-10`   #primeira data
   echo $(( ( $(date -ud $last +'%s') - $(date -ud $first +'%s') )/60/60/24 )) >> "/home/gleison/Projetos/duration.txt"
   cd "/home/gleison/Projetos/"

done < $1

