#!/bin/bash

#recebe lista com o nome dos projetos

while read line 
do 
   cd $line
   tag=`git tag | head -1`
   uri=`git config --get remote.origin.url`
   cd "/home/gleison/CloneFirstTag"
   git clone --branch $tag $uri
   cd "/home/gleison/CloneProjects"
done < $1
