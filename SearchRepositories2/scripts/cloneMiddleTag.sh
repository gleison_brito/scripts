while read line 
do 
   echo $line
   cd $line
   uri=`git config --get remote.origin.url`
  
   tags=(`git tag`)
  
   count=0
   for tag in ${tags[@]};
   do
      dates[$count]=`git log -1 --format=%ai $tag | cut -c1-10`
      count=$((count+1))
   done

   daysAdd=$(( ( $(date -ud ${dates[$((count-1))]} +'%s') - $(date -ud ${dates[0]} +'%s') )/60/60/24/2 ))
   
   dateMiddle=`date --date="${dates[0]} +$daysAdd days" +%Y-%m-%d`

   #echo $dateMiddle	   
   count=0
   for date in ${dates[@]};
   do
     if [ $(date -d $date +%s) -gt $(date -d $dateMiddle +%s) ];
     then
        cd "/home/gleison/CloneMiddleTag"
        git clone --branch ${tags[$((count-1))]} $uri
        #echo "versao meio: ${tags[$((count-1))]}"
        break
     fi 
   count=$((count+1))
   done


   #$HOME/CloneProjects/cloneMiddleTag.sh
   cd "/home/gleison/CloneProjects"
done < $1
