#!/bin/bash

while read line 
do  
   git clone "$line" 
   begin=${line##*/}
   directory=${begin:0: ${#begin}-4}
   echo $directory
   cd $directory
   git log --tags --simplify-by-decoration --pretty="format:%ci %d" | tee ~/$directory"_tags.txt"
   git branch -r | tee ~/$directory"_branchs.txt"
   cd ..
done < $1
