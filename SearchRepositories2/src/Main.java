import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;

import org.apache.http.impl.execchain.RequestAbortedException;

import com.jcabi.github.Github;
import com.jcabi.github.RtGithub;
import com.jcabi.http.Request;
import com.jcabi.http.response.JsonResponse;
import com.opencsv.CSVWriter;

public class Main {

	public static void main(String[] args) throws IOException {
		CSVWriter writer = new CSVWriter(new FileWriter("/home/gleison/systemsJavaScript/data06.csv"));
		
		
		Github github = new RtGithub("gleisonbt", "Aleister93");
		
		Request request;
		JsonArray items;
		int page = 1;
		int cont = 1;
		do {
			request = github.entry().uri().path("/search/repositories")
					.queryParam("q", "language:javascript stars:100..603")
					//.queryParam("q", "user:stoicflame repo:enunciate language:Java")
					.queryParam("sort", "stars")
					.queryParam("per_page", "100")
					.queryParam("page", "" + page)
					//.queryParam("stars", ">=4000")
					.back().method(Request.GET);
			
			System.out.println(request.uri().toString());
			items = request.fetch().as(JsonResponse.class).json().readObject().getJsonArray("items");
			
			for (JsonValue item : items) {
				JsonObject repoData = (JsonObject) item;
				int[] info_contr = contributorsInfo(repoData.getString("contributors_url").substring(23), github);
				
				String line = repoData.getString("name") + ","
						+ repoData.getString("git_url") + ","
						+ repoData.getInt("stargazers_count");// + "," //stars
						//+ whatchersInfo(repoData.getString("subscribers_url").substring(23), github) + "," //whatchers 
						//+ info_contr[0] + "," //contribuitors
						//+ repoData.getInt("forks_count") + "," //forks
						//+ repoData.getInt("size") + "," //size (number of files)
						//+ whatchersInfo(repoData.getString("tags_url").substring(23), github); //tags
		
				writer.writeNext(line.split(","));
				
				System.out.println(cont++ + "" + "\t" + line);
			}
			
			page++;
			
			if (page > 10) {
				break;
			}
			
		} while (items.size() == 100);
		
		
		writer.close();
	}
	
	public static int[] contributorsInfo(String url, Github github) throws IOException {
		
		int commits_count = 0;
		
		Request request2 = github.entry().uri().path(url)
				.queryParam("per_page", "100").back().method(Request.GET);
		
		JsonArray  contributors_page = request2.fetch().as(JsonResponse.class).json().readArray();
		for (JsonValue contribuitor : contributors_page) {
			JsonObject contr = (JsonObject) contribuitor;
			commits_count+=contr.getInt("contributions");
		}
		
		int contribuitors_count_page = contributors_page.size();
		int contribuitors_count = contribuitors_count_page;
		
	
		int cont = 2;
		while (contribuitors_count_page == 100) {
			contribuitors_count+=contribuitors_count_page;
			request2 = github.entry().uri().path(url)
					.queryParam("per_page", "100").queryParam("page", "" + cont).back().method(Request.GET);
			contributors_page = request2.fetch().as(JsonResponse.class).json().readArray();
			for (JsonValue contribuitor : contributors_page) {
				JsonObject contr = (JsonObject) contribuitor;
				commits_count+=contr.getInt("contributions");
			}
			contribuitors_count_page = contributors_page.size();
			cont++;
		}
		
		int[] results = {contribuitors_count, commits_count};
		
		return results;
	}
	
	
	public static int whatchersInfo(String url, Github github) throws IOException {
		int result = 0;
		int contPage = 1;
		
		Request request  = github.entry().uri().path(url).queryParam("per_page", "100").queryParam("page", ""+contPage).back().method(Request.GET);
		 
		
			JsonArray page = request.fetch().as(JsonResponse.class).json().readArray();
		
		
		while (!page.isEmpty()) {
			//System.out.println(request.uri());
			result+=page.size();
			contPage++;
			request = github.entry().uri().path(url).queryParam("per_page", "100").queryParam("page", ""+contPage).back().method(Request.GET);
			try {
				page = request.fetch().as(JsonResponse.class).json().readArray();
			} catch (Exception e) {
				
			}
			
		}
		
		
		return result;
		/*Request request2 = github.entry().uri().path(url)
				.queryParam("per_page", "100").back().method(Request.GET);
		
		
		
		//System.out.println("consulta: " + request2.uri());
		
		JsonArray  whatchers_page = request2.fetch().as(JsonResponse.class).json().readArray();
		
		int whatchers_count_page = whatchers_page.size();
		int whatchers_count = whatchers_count_page;
		
	
		int cont = 2;
		while (whatchers_count_page != 0) {
			whatchers_count+=whatchers_count_page;
			request2 = github.entry().uri().path(url)
					.queryParam("per_page", "100").queryParam("page", "" + cont).back().method(Request.GET);
			System.out.println("consulta: " + request2.uri());
			try {
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			//whatchers_page = request2.fetch().as(JsonResponse.class).json().readArray();
			whatchers_count_page = request2.fetch().as(JsonResponse.class).json().readArray().size();
			cont++;
		}
		
		return whatchers_count;*/
		
	}
	
	public static int tagsInfo(String url, Github github) throws IOException {
		Request request2 = github.entry().uri().path(url)
				.queryParam("per_page", "100").back().method(Request.GET);
		
		JsonArray  tags_page = request2.fetch().as(JsonResponse.class).json().readArray();
		
		int tags_count_page = tags_page.size();
		int whatchers_count = tags_count_page;
		
	
		int cont = 2;
		while (tags_count_page != 0) {
			whatchers_count+=tags_count_page;
			request2 = github.entry().uri().path(url)
					.queryParam("per_page", "100").queryParam("page", "" + cont).back().method(Request.GET);
			tags_page = request2.fetch().as(JsonResponse.class).json().readArray();
			tags_count_page = tags_page.size();
			cont++;
		}
		
		return whatchers_count;
	}

	public static int branchesInfo(String url, Github github) throws IOException {
		Request request2 = github.entry().uri().path(url)
				.queryParam("per_page", "100").back().method(Request.GET);
		
		JsonArray  branches_page = request2.fetch().as(JsonResponse.class).json().readArray();
		
		int branches_count_page = branches_page.size();
		int branches_count = branches_count_page;
		
	
		int cont = 2;
		while (branches_count_page != 0) {
			branches_count+=branches_count_page;
			request2 = github.entry().uri().path(url)
					.queryParam("per_page", "100").queryParam("page", "" + cont).back().method(Request.GET);
			branches_page = request2.fetch().as(JsonResponse.class).json().readArray();
			branches_count_page = branches_page.size();
			cont++;
		}
		
		return branches_count;
	}
	
}
